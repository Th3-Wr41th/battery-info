// SPDX-License-Identfier: MPL-2.0

#![deny(clippy::correctness, clippy::suspicious)]
#![warn(clippy::complexity, clippy::perf, clippy::style, clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]

#[cfg(not(target_os = "linux"))]
compile_error!("target os not supported");

#[macro_use]
extern crate tracing;

mod dbus;
mod sections;
mod upower;

use bat::{
    config::Config,
    style::{StyleComponent, StyleComponents},
};
use color_eyre::eyre::{self, Context as _};
use console::Term;
use once_cell::sync::Lazy;
use sections::General;
use tracing::level_filters::LevelFilter;
use zbus::Connection;

use crate::sections::Batteries;

static PRINTER_CONFIG: Lazy<Config> = Lazy::new(|| {
    let colors_enabled = console::colors_enabled();

    Config {
        colored_output: colors_enabled,
        true_color: colors_enabled,
        term_width: Term::stdout().size().1 as usize,
        style_components: StyleComponents::new(&[
            StyleComponent::HeaderFilename,
            StyleComponent::Grid,
            StyleComponent::LineNumbers,
        ]),
        ..Default::default()
    }
});

#[macro_export]
macro_rules! pretty_write {
    ($buf:expr, $prefix:expr) => {
        writeln!(
            $buf,
            "{0}{1}",
            console::style($prefix).dim(),
            console::style(":").dim()
        )
    };

    ($buf:expr, $prefix:expr, $args:expr) => {
        writeln!(
            $buf,
            "{0}{1} {2}",
            console::style($prefix).dim(),
            console::style(":").dim(),
            $args,
        )
    };

    ($buf:expr, $indent:expr, $prefix:expr, $args:expr) => {
        writeln!(
            $buf,
            "{0}{1}{2} {3}",
            $indent,
            console::style($prefix).dim(),
            console::style(":").dim(),
            $args
        )
    };
}

fn init_tracing() -> eyre::Result<()> {
    #[cfg(debug_assertions)]
    let level = LevelFilter::DEBUG;

    #[cfg(not(debug_assertions))]
    let level = LevelFilter::WARN;

    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(level)
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("setting default subscriber failed")?;

    info!("tracing started");

    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> eyre::Result<()> {
    color_eyre::install()?;
    init_tracing()?;

    let conn = Connection::system().await?;

    let mut output = String::new();

    let general = General::new(&conn).await?;

    general.print(&mut output).await?;

    let batteries = Batteries::new(&conn).await?;

    batteries.print(&mut output).await?;

    print!("{output}");

    Ok(())
}
