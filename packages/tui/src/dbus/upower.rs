// SPDX-License-Identfier: MPL-2.0

use zbus::{proxy, zvariant};

#[proxy(
    interface = "org.freedesktop.UPower",
    default_service = "org.freedesktop.UPower",
    default_path = "/org/freedesktop/UPower"
)]
trait UPower {
    fn enumerate_devices(&self) -> zbus::Result<Vec<zvariant::OwnedObjectPath>>;

    fn get_display_device(&self) -> zbus::Result<zvariant::OwnedObjectPath>;

    #[zbus(property)]
    fn daemon_version(&self) -> zbus::Result<String>;

    #[zbus(property)]
    fn on_battery(&self) -> zbus::Result<bool>;
}
