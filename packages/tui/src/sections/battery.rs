// SPDX-License-Identfier: MPL-2.0

#![allow(clippy::cast_sign_loss, clippy::cast_possible_truncation)]

use bat::{assets::HighlightingAssets, controller::Controller, Input};
use color_eyre::eyre;
use console::style;
use hhmmss::Hhmmss;
use std::{io::Write, time::Duration};
use zbus::Connection;

use crate::{
    pretty_write,
    upower::{BatteryLevel, DeviceType, State, UPower, UPowerDeviceProperties},
    PRINTER_CONFIG,
};

#[derive(Debug)]
pub struct Batteries<'a>(UPower<'a>);

impl<'a> Batteries<'a> {
    pub async fn new(conn: &Connection) -> eyre::Result<Self> {
        let upower = UPower::new(conn).await?;

        Ok(Self(upower))
    }

    pub async fn print<W: std::fmt::Write>(&self, buffer: &mut W) -> eyre::Result<()> {
        let devices = self.0.devices().await?;

        for device in &devices {
            let properties = device.properties().await?;

            if !((properties.device_type == DeviceType::Battery) & properties.power_supply) {
                continue;
            }

            self.print_device(&properties, buffer)?;
        }

        Ok(())
    }

    #[allow(clippy::unused_self)]
    fn print_device<W: std::fmt::Write>(
        &self,
        properties: &UPowerDeviceProperties,
        buffer: &mut W,
    ) -> eyre::Result<()> {
        let mut b = Vec::new();
        let indent = " ".repeat(2);

        pretty_write!(&mut b, "Status", format_args!("{0}", properties.status))?;

        writeln!(&mut b)?;

        pretty_write!(&mut b, "State", format_args!("{0}", properties.state))?;

        if properties.state == State::Charging {
            let time = Duration::from_secs(properties.time_to_full as u64).hhmmss();
            writeln!(&mut b, "{indent}Time Remaining: {time}")?;
        } else if properties.state == State::Discharging {
            let time = Duration::from_secs(properties.time_to_empty as u64).hhmmss();
            writeln!(&mut b, "{indent}Time Remaining: {time}")?;
        }
        writeln!(&mut b)?;

        pretty_write!(&mut b, "General Details")?;

        pretty_write!(
            &mut b,
            indent,
            "Model",
            format_args!(
                "{0}-{1} [{2}]",
                properties.vendor, properties.model, properties.serial
            )
        )?;

        pretty_write!(
            &mut b,
            indent,
            "Technology",
            format_args!(
                "{0} [{1}]",
                properties.technology, properties.is_rechargeable
            )
        )?;

        pretty_write!(
            &mut b,
            indent,
            "Voltage",
            format_args!("{0:.1}v", properties.voltage)
        )?;

        writeln!(&mut b)?;

        let percentage: String = if properties.battery_level == BatteryLevel::None {
            format!("{0:.0}%", properties.percentage)
        } else {
            properties.battery_level.to_string()
        };

        pretty_write!(&mut b, "Charge", format_args!("{percentage}"))?;

        // Limit with to 50 chars
        let current = (properties.percentage as u64) / 2;

        write!(&mut b, "{indent}")?;

        for _ in 0..current {
            write!(&mut b, "{0}", style("▌").green())?;
        }

        for _ in current..50 {
            write!(&mut b, "{0}", style("▌").black().dim())?;
        }

        writeln!(&mut b)?;
        writeln!(&mut b)?;

        let health = (properties.energy.full / properties.energy.full_design) * 100.0;

        pretty_write!(&mut b, "Health", format_args!("{health:.0}%"))?;

        // Limit with to 50 chars
        let current = (health as u64) / 2;

        write!(&mut b, "{indent}")?;

        for _ in 0..current {
            write!(&mut b, "{0}", style("▌").black().dim())?;
        }

        for _ in current..50 {
            write!(&mut b, "{0}", style("▌").red())?;
        }

        writeln!(&mut b)?;

        Controller::new(&PRINTER_CONFIG, &HighlightingAssets::from_binary()).run(
            vec![Input::from_bytes(&b)
                .kind("Battery")
                .title(properties.path.as_ref())
                .into()],
            Some(buffer),
        )?;

        Ok(())
    }
}
