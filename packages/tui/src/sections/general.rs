// SPDX-License-Identfier: MPL-2.0

use bat::{assets::HighlightingAssets, controller::Controller, Input};
use color_eyre::eyre;
use std::io::Write;
use zbus::Connection;

use crate::{
    pretty_write,
    upower::{DeviceType, State, UPower},
    PRINTER_CONFIG,
};

#[derive(Debug)]
pub struct General<'a>(UPower<'a>);

impl<'a> General<'a> {
    pub async fn new(conn: &Connection) -> eyre::Result<Self> {
        let upower = UPower::new(conn).await?;

        Ok(Self(upower))
    }

    pub async fn print<W: std::fmt::Write>(&self, buffer: &mut W) -> eyre::Result<()> {
        let properties = self.0.properties().await?;

        let display_device = self.0.display_device().await?;
        let dd_properties = display_device.properties().await?;

        let device_in_use: Option<_> = 'outer: {
            let devices = self.0.devices().await?;

            let mut discharging: Option<DeviceType> = None;

            for device in devices {
                let properties = device.properties().await?;

                if properties.online {
                    break 'outer Some(properties.device_type);
                }

                if properties.state == State::Discharging {
                    discharging = Some(properties.device_type);
                }
            }

            discharging
        };

        let mut b = Vec::new();

        pretty_write!(
            &mut b,
            "UPower Daemon Version",
            format_args!("{0}", properties.daemon_version)
        )?;

        writeln!(&mut b)?;

        pretty_write!(&mut b, "State", format_args!("{0}", dd_properties.state))?;

        pretty_write!(
            &mut b,
            "Percentage",
            format_args!("{0}%", dd_properties.percentage)
        )?;

        if let Some(fmt) = device_in_use {
            writeln!(&mut b)?;
            pretty_write!(&mut b, "In Use", format_args!("{fmt}"))?;
        }

        Controller::new(&PRINTER_CONFIG, &HighlightingAssets::from_binary()).run(
            vec![Input::from_bytes(&b).title("General").into()],
            Some(buffer),
        )?;

        Ok(())
    }
}
