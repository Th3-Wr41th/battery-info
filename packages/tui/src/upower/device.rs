// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, Context as _};
use derive_more::Display;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use std::{fmt, sync::Arc};
use zbus::{zvariant, Connection};

use crate::dbus::upower_device::UPowerDeviceProxy;

#[derive(Debug, Clone)]
pub struct UPowerDevice<'a> {
    proxy: UPowerDeviceProxy<'a>,
}

impl<'a> UPowerDevice<'a> {
    pub async fn new(conn: &Connection, path: zvariant::OwnedObjectPath) -> eyre::Result<Self> {
        info!("creating new proxy");

        let proxy = UPowerDeviceProxy::new(conn, path)
            .await
            .context("failed to establish connection to UPowerDevice")?;

        Ok(Self { proxy })
    }

    pub async fn properties(&self) -> eyre::Result<UPowerDeviceProperties> {
        info!("getting properties");

        let device_type = self.proxy.__type().await?;
        let state = self.proxy.state().await?;
        let technology = self.proxy.technology().await?;
        let battery_level = self.proxy.battery_level().await?;

        let properties = UPowerDeviceProperties {
            path: self.proxy.native_path().await?.into(),
            vendor: self.proxy.vendor().await?.into(),
            model: self.proxy.model().await?.into(),
            serial: self.proxy.serial().await?.into(),
            device_type: DeviceType::from_u32(device_type).unwrap_or(DeviceType::Unknown),
            power_supply: self.proxy.power_supply().await?,
            online: self.proxy.online().await?,
            energy: Energy {
                current: self.proxy.energy().await?,
                empty: self.proxy.energy_empty().await?,
                full: self.proxy.energy_full().await?,
                full_design: self.proxy.energy_full_design().await?,
                rate: self.proxy.energy_rate().await?,
            },
            voltage: self.proxy.voltage().await?,
            time_to_empty: self.proxy.time_to_empty().await?,
            time_to_full: self.proxy.time_to_full().await?,
            percentage: self.proxy.percentage().await?,
            status: self.proxy.is_present().await?.into(),
            state: State::from_u32(state).unwrap_or(State::Unknown),
            is_rechargeable: self.proxy.is_rechargeable().await?.into(),
            technology: Technology::from_u32(technology).unwrap_or(Technology::Unknown),
            battery_level: BatteryLevel::from_u32(battery_level).unwrap_or(BatteryLevel::Unknown),
        };

        Ok(properties)
    }
}

#[derive(Debug, Clone)]
pub struct UPowerDeviceProperties {
    pub path: Arc<str>,
    pub vendor: Arc<str>,
    pub model: Arc<str>,
    pub serial: Arc<str>,
    pub device_type: DeviceType,
    pub power_supply: bool,
    pub online: bool,
    pub energy: Energy,
    pub voltage: f64,
    pub time_to_empty: i64,
    pub time_to_full: i64,
    pub percentage: f64,
    pub status: Status,
    pub state: State,
    pub is_rechargeable: IsRechargeable,
    pub technology: Technology,
    pub battery_level: BatteryLevel,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive)]
pub enum DeviceType {
    Unknown = 0,
    LinePower = 1,
    Battery = 2,
}

impl fmt::Display for DeviceType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Unknown => write!(f, "Unknown"),
            Self::LinePower => write!(f, "Line Power"),
            Self::Battery => write!(f, "Battery"),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Energy {
    pub current: f64,
    pub empty: f64,
    pub full: f64,
    pub full_design: f64,
    pub rate: f64,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive)]
pub enum State {
    Unknown = 0,
    Charging = 1,
    Discharging = 2,
    Empty = 3,
    FullyCharged = 4,
    PendingCharge = 5,
    PendingDischarge = 6,
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Unknown => write!(f, "Unknown"),
            Self::Charging => write!(f, "Charging"),
            Self::Discharging => write!(f, "Discharging"),
            Self::Empty => write!(f, "Empty"),
            Self::FullyCharged => write!(f, "Fully Charged"),
            Self::PendingCharge => write!(f, "Pending Charge"),
            Self::PendingDischarge => write!(f, "Pending Discharge"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive)]
pub enum Technology {
    Unknown = 0,
    LithiumIon = 1,
    LithiumPolymer = 2,
    LithiumIronPhosphate = 3,
    LeadAcid = 4,
    NickelCadmium = 5,
    NickelMetalHydride = 6,
}

impl fmt::Display for Technology {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Unknown => write!(f, "Unknown"),
            Self::LithiumIon => write!(f, "Lithium Ion (LiIon)"),
            Self::LithiumPolymer => write!(f, "Lithium Polymer (LiPo)"),
            Self::LithiumIronPhosphate => write!(f, "Lithium Iron Phosphate (LiFePO)"),
            Self::LeadAcid => write!(f, "Lead Acid"),
            Self::NickelCadmium => write!(f, "Nickel Cadmium (NiCd)"),
            Self::NickelMetalHydride => write!(f, "Nickel Metal Hydride (NiMH)"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive, Display)]
pub enum BatteryLevel {
    Unknown = 0,
    None = 1,
    Low = 3,
    Critical = 4,
    Normal = 6,
    High = 7,
    Full = 8,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Display)]
pub enum Status {
    Present,
    Missing,
}

impl From<bool> for Status {
    fn from(value: bool) -> Self {
        if value {
            Self::Present
        } else {
            Self::Missing
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum IsRechargeable {
    Rechargeable,
    NonRechargeable,
}

impl fmt::Display for IsRechargeable {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Rechargeable => write!(f, "Rechargeable"),
            Self::NonRechargeable => write!(f, "Non-Rechargeable"),
        }
    }
}

impl From<bool> for IsRechargeable {
    fn from(value: bool) -> Self {
        if value {
            Self::Rechargeable
        } else {
            Self::NonRechargeable
        }
    }
}
