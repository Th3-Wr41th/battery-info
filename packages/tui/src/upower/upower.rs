// SPDX-License-Identfier: MPL-2.0

use std::sync::Arc;

use color_eyre::eyre::{self, Context as _};
use zbus::Connection;

use crate::dbus::upower::UPowerProxy;

use super::UPowerDevice;

#[derive(Debug)]
pub struct UPower<'a> {
    connection: Connection,
    proxy: UPowerProxy<'a>,
}

impl<'a> UPower<'a> {
    pub async fn new(conn: &Connection) -> eyre::Result<Self> {
        info!("creating new proxy");

        let proxy = UPowerProxy::new(conn)
            .await
            .context("failed to establish proxy to UPower")?;

        Ok(Self {
            connection: conn.clone(),
            proxy,
        })
    }

    pub async fn display_device(&self) -> eyre::Result<UPowerDevice> {
        info!("getting display device");

        let path = self
            .proxy
            .get_display_device()
            .await
            .context("failed to get display device")?;

        let device = UPowerDevice::new(&self.connection, path).await?;

        Ok(device)
    }

    pub async fn devices(&self) -> eyre::Result<Vec<UPowerDevice>> {
        info!("getting devices");

        let paths = self
            .proxy
            .enumerate_devices()
            .await
            .context("failed to enumerate devices")?;

        let mut devices: Vec<UPowerDevice> = Vec::with_capacity(paths.len());

        for path in paths {
            let device = UPowerDevice::new(&self.connection, path).await?;

            devices.push(device);
        }

        Ok(devices)
    }

    pub async fn properties(&self) -> eyre::Result<UPowerProperties> {
        info!("getting properties");

        let version = self
            .proxy
            .daemon_version()
            .await
            .context("failed to get property: daemon_version")?;

        let on_battery = self
            .proxy
            .on_battery()
            .await
            .context("failed to get property: on_battery")?;

        Ok(UPowerProperties {
            daemon_version: version.into(),
            on_battery,
        })
    }
}

#[derive(Debug, Clone)]
pub struct UPowerProperties {
    pub daemon_version: Arc<str>,
    pub on_battery: bool,
}
