// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, eyre};
use futures::stream::StreamExt;
use notify_rust::{Notification, Timeout};
use num_traits::FromPrimitive;
use winit::event_loop::EventLoopProxy;
use zbus::{
    fdo::{PropertiesChangedArgs, PropertiesChangedStream, PropertiesProxy},
    proxy,
    zvariant::Value,
    Connection,
};

use crate::{
    chain,
    tray::{IconArgs, State},
    AppEvent,
};

const DESTINATION: &str = "org.freedesktop.UPower";
const PATH: &str = "/org/freedesktop/UPower/devices/DisplayDevice";

pub struct Monitor<'a> {
    proxy: PropertiesProxy<'a>,
    device: DisplayDeviceProxy<'a>,
}

impl<'a> Monitor<'a> {
    pub async fn init() -> eyre::Result<Self> {
        let conn = Connection::system().await?;

        let proxy: PropertiesProxy = PropertiesProxy::builder(&conn)
            .destination(DESTINATION)?
            .path(PATH)?
            .build()
            .await?;

        let device: DisplayDeviceProxy = DisplayDeviceProxy::new(&conn).await?;

        Ok(Self { proxy, device })
    }

    pub async fn monitor(&self, tx: &EventLoopProxy<AppEvent>) -> eyre::Result<()> {
        let mut stream: PropertiesChangedStream = self.proxy.receive_properties_changed().await?;

        while let Some(change) = stream.next().await {
            let args: PropertiesChangedArgs = change.args().map_err(|err| eyre!("{err}"))?;

            if let Some(Value::Str(val)) = args.changed_properties.get("IconName") {
                let name: &str = val.as_str();

                if let Err(err) = self.update_icon(tx, name).await {
                    chain!(error!(err));
                }
            }

            if let Some(Value::U32(val)) = args.changed_properties.get("State") {
                let state: State = State::from_u32(*val).unwrap_or(State::Unknown);

                let body = match state {
                    State::FullyCharged => Some("Your battery is now fully charged"),
                    State::Discharging => Some(
                        "The AC Power has been unplugged. The system is now using battery power",
                    ),
                    _ => None,
                };

                if let Some(body) = body {
                    let mut notification = Notification::new();

                    notification.summary("Battery Info");
                    notification.body(body);
                    notification.timeout(Timeout::Milliseconds(5000));

                    tokio::spawn(async move {
                        if let Err(err) = notification.show_async().await {
                            error!("{err}");
                        }
                    });
                }
            }
        }

        Ok(())
    }

    pub async fn update_icon(&self, tx: &EventLoopProxy<AppEvent>, name: &str) -> eyre::Result<()> {
        let percentage = self.device.percentage().await?;

        let state = self.device.state().await?;
        let state: State = State::from_u32(state).unwrap_or(State::Unknown);

        let args = IconArgs {
            name: name.into(),
            percentage,
            state,
        };

        tx.send_event(AppEvent::UpdateIcon(args))?;

        Ok(())
    }
}

#[proxy(
    default_service = "org.freedesktop.UPower",
    default_path = "/org/freedesktop/UPower/devices/DisplayDevice",
    interface = "org.freedesktop.UPower.Device"
)]
trait DisplayDevice {
    #[zbus(property)]
    fn percentage(&self) -> zbus::Result<f64>;

    #[zbus(property)]
    fn state(&self) -> zbus::Result<u32>;
}
