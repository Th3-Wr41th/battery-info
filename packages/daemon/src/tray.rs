// SPDX-License-Identfier: MPL-2.0

use std::sync::Arc;

use async_channel::Receiver;
use color_eyre::eyre::{self, Context as _};
use num_derive::FromPrimitive;
use tray_icon::TrayIconBuilder;

use crate::{chain, icon::Icon};

#[derive(Debug)]
pub struct IconArgs {
    pub name: Arc<str>,
    pub state: State,
    pub percentage: f64,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive)]
pub enum State {
    Unknown = 0,
    Charging = 1,
    Discharging = 2,
    Empty = 3,
    FullyCharged = 4,
    PendingCharge = 5,
    PendingDischarge = 6,
}

pub fn spawn(rx: Receiver<IconArgs>) -> eyre::Result<()> {
    let icon = Icon::default_icon()?;

    gtk::init().context("failed to initialize gtk")?;

    let tray = TrayIconBuilder::new()
        .with_icon(icon.as_ref().clone())
        .build()?;

    glib::MainContext::default().spawn_local(async move {
        while let Ok(icon) = rx.recv().await {
            if let Some(icon) = Icon::contextual(icon.state, icon.percentage) {
                if let Err(err) = tray.set_icon(Some(icon.as_ref().clone())) {
                    error!("{err}");
                }

                continue;
            }

            match Icon::load(icon.name.as_ref()) {
                Ok(icon) => {
                    if let Err(err) = tray.set_icon(Some(icon.as_ref().clone())) {
                        error!("{err}");
                    }
                }
                Err(err) => {
                    let err = err.wrap_err(format!("failed to load icon: {0}", icon.name));

                    chain!(warn!(err));
                }
            }
        }
    });

    gtk::main();

    Ok(())
}
