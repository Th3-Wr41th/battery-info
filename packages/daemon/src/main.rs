// SPDX-License-Identfier: MPL-2.0

#![deny(clippy::correctness, clippy::suspicious)]
#![warn(clippy::complexity, clippy::perf, clippy::style, clippy::pedantic)]

#[cfg(not(target_os = "linux"))]
compile_error!("target os not supported");

#[macro_use]
extern crate tracing;

mod dbus;
mod icon;
mod tray;

use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

use color_eyre::eyre::{self, Context as _};
use tracing::level_filters::LevelFilter;
use tray::IconArgs;
use winit::{
    event,
    event_loop::{ControlFlow, EventLoopBuilder},
};

use crate::dbus::Monitor;

#[macro_export]
macro_rules! chain {
    ($level:ident!($err:expr)) => {{
        let mut chain = $err.chain();

        if let Some(tlm) = chain.next() {
            $level!("{tlm}");
        }

        for cause in chain {
            $level!("caused by: {cause}");
        }
    }};
}

fn init_tracing() -> eyre::Result<()> {
    #[cfg(debug_assertions)]
    {
        let subscriber = tracing_subscriber::FmtSubscriber::builder()
            .with_max_level(LevelFilter::DEBUG)
            .finish();

        tracing::subscriber::set_global_default(subscriber)
            .context("setting default subscriber failed")?;
    }

    #[cfg(not(debug_assertions))]
    {
        use tracing_subscriber::{
            layer::{Layer, SubscriberExt},
            util::SubscriberInitExt,
        };

        let journald =
            tracing_journald::Layer::new().context("failed to contruct journald layer")?;

        let journald = journald
            .with_syslog_identifier("battery-info-daemon".to_string())
            .with_filter(LevelFilter::WARN);

        tracing_subscriber::registry().with(journald).init();
    }

    info!("tracing started");

    Ok(())
}

#[derive(Debug)]
enum AppEvent {
    UpdateIcon(IconArgs),
}

#[tokio::main]
async fn main() -> eyre::Result<()> {
    color_eyre::install()?;
    init_tracing()?;

    let is_terminated = Arc::new(AtomicBool::new(false));
    let terminated = Arc::clone(&is_terminated);
    let gtk_terminated = Arc::clone(&is_terminated);

    ctrlc::set_handler(move || {
        info!("beginning graceful shutdown");
        terminated.store(true, Ordering::SeqCst);
    })
    .context("failed to set ctrlc handler")?;

    let (tx, rx) = async_channel::bounded(10);

    std::thread::spawn(move || {
        if let Err(err) = tray::spawn(rx) {
            error!("{err}");

            gtk_terminated.store(true, Ordering::SeqCst);

            panic!("{err}");
        }
    });

    let event_loop = EventLoopBuilder::<AppEvent>::with_user_event().build()?;
    let event_loop_proxy = event_loop.create_proxy();

    tokio::spawn(async move {
        let monitor = match Monitor::init().await {
            Ok(val) => val,
            Err(err) => {
                chain!(error!(err));

                panic!("failed to initialize monitor");
            }
        };

        if let Err(err) = monitor
            .update_icon(&event_loop_proxy, "battery-full-symbolic")
            .await
        {
            chain!(error!(err));
        }

        if let Err(err) = monitor.monitor(&event_loop_proxy).await {
            chain!(error!(err));
        }
    });

    event_loop
        .run(move |event, event_loop| {
            event_loop.set_control_flow(ControlFlow::Poll);

            #[cfg(debug_assertions)]
            if let event::Event::NewEvents(event::StartCause::Init) = event {
                info!("event loop started");
            }

            if let event::Event::UserEvent(AppEvent::UpdateIcon(args)) = event {
                if let Err(err) = tx.send_blocking(args) {
                    error!("{err}");

                    is_terminated.store(true, Ordering::SeqCst);
                }
            }

            if is_terminated.load(Ordering::SeqCst) {
                event_loop.exit();
            }
        })
        .context("failed to run event loop")?;

    Ok(())
}
