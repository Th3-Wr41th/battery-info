// SPDX-License-Identfier: MPL-2.0

use std::{fs, path::Path};

use color_eyre::eyre::{self, bail, eyre, Context, ContextCompat};
use image::ImageFormat;
use linicon::{IconPath, IconType, LiniconError};
use resvg::{
    tiny_skia,
    usvg::{self, fontdb, Tree},
};

use crate::tray::State;

const DEFAULT_ICON: &[u8; 2938] = include_bytes!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/assets/default-tray-icon.png"
));

#[derive(Debug)]
pub struct Icon(tray_icon::Icon);

impl Icon {
    pub fn default_icon() -> eyre::Result<Self> {
        let image =
            image::load_from_memory_with_format(DEFAULT_ICON, ImageFormat::Png)?.into_rgba8();

        let (width, height) = image.dimensions();

        let rgba = image.into_raw();

        let icon =
            tray_icon::Icon::from_rgba(rgba, width, height).context("failed to load icon")?;

        Ok(Self(icon))
    }

    pub fn load(name: &str) -> eyre::Result<Self> {
        let Some(theme) = linicon::get_system_theme() else {
            bail!("unable to get system theme");
        };

        let icons: Vec<_> = linicon::lookup_icon(name)
            .from_theme(theme)
            .use_fallback_themes(false)
            .collect::<Result<Vec<IconPath>, LiniconError>>()
            .context(format!("cannot find icon: {name}"))?;

        let icon: Option<_> = icons
            .iter()
            .find(|icon| {
                let icon = icon.path.to_string_lossy();

                icon.contains("panel")
            })
            .or(icons.first());

        if let Some(icon) = icon {
            let icon = match icon.icon_type {
                IconType::SVG => {
                    let bytes = Self::render_svg(&icon.path)?;

                    let image = {
                        let tmp = image::load_from_memory_with_format(&bytes, ImageFormat::Png)?;
                        tmp.into_rgba8()
                    };

                    let (width, height) = image.dimensions();

                    let rgba = image.into_raw();

                    tray_icon::Icon::from_rgba(rgba, width, height)
                        .context("failed to load icon")?
                }
                IconType::PNG => {
                    let mut image = image::io::Reader::open(&icon.path)?;

                    image.set_format(ImageFormat::Png);

                    let image = {
                        let tmp = image.decode().context("unsupported image format")?;
                        tmp.into_rgba8()
                    };

                    let (width, height) = image.dimensions();

                    let rgba = image.into_raw();

                    tray_icon::Icon::from_rgba(rgba, width, height)
                        .context("failed to load icon")?
                }
                IconType::XMP => bail!("icon type not supported: {name}"),
            };

            Ok(Self(icon))
        } else {
            Err(eyre!("cannot find icon: {name}"))
        }
    }

    pub fn contextual(state: State, percentage: f64) -> Option<Self> {
        let icon = Self::get_contextual_name(state, percentage);

        Self::load(&icon).ok()
    }

    fn get_contextual_name(state: State, percentage: f64) -> String {
        #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
        let percentage: u64 = percentage.floor() as u64;

        macro_rules! icon_name {
            ($level:expr, $charging:expr) => {
                if $charging {
                    format!("battery-level-{0}-charging-symbolic", $level)
                } else {
                    format!("battery-level-{0}-symbolic", $level)
                }
            };
        }

        macro_rules! string {
            ($what:expr) => {
                String::from($what)
            };
        }

        let charging: bool = state == State::Charging;

        match state {
            State::Charging
            | State::Discharging
            | State::PendingCharge
            | State::PendingDischarge => match percentage {
                0..=9 => icon_name!(0, charging),
                10..=19 => icon_name!(10, charging),
                20..=29 => icon_name!(20, charging),
                30..=39 => icon_name!(30, charging),
                40..=49 => icon_name!(40, charging),
                50..=59 => icon_name!(50, charging),
                60..=69 => icon_name!(60, charging),
                70..=79 => icon_name!(70, charging),
                80..=89 => icon_name!(80, charging),
                90..=99 => icon_name!(90, charging),
                100.. => icon_name!(100, charging),
            },
            State::FullyCharged => string!("battery-full-charged-symbolic"),
            State::Empty => string!("battery-empty-symbolic"),
            State::Unknown => string!("battery-missing-symbolic"),
        }
    }

    fn render_svg<P: AsRef<Path>>(path: P) -> eyre::Result<Vec<u8>> {
        let tree: Tree = {
            let opt = usvg::Options {
                resources_dir: fs::canonicalize(format!("/tmp/{0}", env!("CARGO_PKG_NAME"))).ok(),
                ..Default::default()
            };

            let mut fontdb = fontdb::Database::new();
            fontdb.load_system_fonts();

            let svg_data = fs::read(path).context("failed to read icon data from path")?;

            Tree::from_data(&svg_data, &opt, &fontdb).context("failed to construct usvg tree")?
        };

        let pixmap_size = tree.size().to_int_size();
        let mut pixmap = tiny_skia::Pixmap::new(pixmap_size.width(), pixmap_size.height())
            .context("failed to construct pixmap")?;

        resvg::render(&tree, tiny_skia::Transform::default(), &mut pixmap.as_mut());

        let png_data = pixmap
            .encode_png()
            .context("failed to encode pixmal as png")?;

        Ok(png_data)
    }
}

impl AsRef<tray_icon::Icon> for Icon {
    fn as_ref(&self) -> &tray_icon::Icon {
        &self.0
    }
}
