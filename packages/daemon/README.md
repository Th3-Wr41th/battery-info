# battery-info: daemon

## Dependencies
This program utilizes [tray-icon](https://crates.io/crates/tray-icon) which has
its own [dependencies](https://crates.io/crates/tray-icon#dependencies-linux-only)
