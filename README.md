# battery-info

This project contains both a tray app for monitoring battery level and notifing
of AC attach or detach, as well a console interface to get detailed information
regarding battery status and health.

## Licensing

See packages for their respective licenses, copies of all licenses can be found
in the root of the project.
